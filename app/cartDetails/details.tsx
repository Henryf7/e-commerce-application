'use client';
import { useEffect, useState } from 'react';
import { CartDetailProps, ICartDetail, ICartDetailProduct, IProduct } from '@/utils/interfaces/interfaces';
import { useRouter } from 'next/navigation';
import { updateCartDetail } from '../services/products';

const CartDetail: React.FC<CartDetailProps> = (props: CartDetailProps) => {
    const router = useRouter();
    const { cartDetailData, productsData } = props;
    const [totalPriceProducts, setTotalPriceProducts] = useState<number>(0);
    const [combinedData, setCombinedData] = useState<ICartDetailProduct[]>([]);

    useEffect(() => {
        const dataCombined: ICartDetailProduct[] = [];
        cartDetailData.forEach(cartDetail => {
            const product = productsData.find(product => product.id === cartDetail.product_id);
            if (product) {
                dataCombined.push({ cartDetail, product });
            }
        });

        setCombinedData(dataCombined);

        const total = dataCombined.reduce((total, { product, cartDetail }) => {
            return total + product.price * cartDetail.quantity;
        }, 0);
        setTotalPriceProducts(total);
    }, []);

    const handleCancel = () => {
        router.push('/e-commerce', { scroll: false });
    }

    const editQuantity = async (detail: ICartDetail,  isAddition: boolean) => {
        if (isAddition) {
            await updateCartDetail(detail.cart_id,detail.product_id, detail.quantity+1);
        } else {
            await updateCartDetail(detail.cart_id,detail.product_id, detail.quantity-1);
        }
    }


    return (
        <div>
            <button
                type="button"
                className="my-4 inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                onClick={handleCancel}>
                To Catalog
            </button>
            <div className="w-full max-w-md p-4 bg-white border border-gray-200 rounded-lg shadow sm:p-8 dark:bg-gray-800 dark:border-gray-700">
                <div className="flex items-center justify-between mb-4">
                    <h5 className="text-xl font-bold leading-none text-gray-900 dark:text-white">Products Cart</h5>
                    <a href="#" className="text-sm font-medium text-blue-600 hover:underline dark:text-blue-500">
                        View all
                    </a>
                </div>
                <div className="flow-root">
                    <ul role="list" className="divide-y divide-gray-200 dark:divide-gray-700">
                        {combinedData.map(cd => {
                            return (
                                <li className="py-3 sm:py-4">
                                    <div className="flex items-center">
                                        <div className="flex-shrink-0">
                                            <img className="w-8 h-8" src={cd.product.img_url} alt={cd.product.name} />
                                        </div>
                                        <div className="flex-1 min-w-0 ms-4">
                                            <p className="text-sm font-medium text-gray-900 truncate dark:text-white">
                                                {cd.product.name}
                                            </p>
                                            <p className="text-sm text-gray-500 truncate dark:text-gray-400">
                                                Quantity = {cd.cartDetail.quantity}
                                            </p>
                                        </div>
                                        <div className="ml-4 inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                                            C$ {cd.product.price}
                                        </div>
                                        <button onClick={() => editQuantity(cd.cartDetail,true)} type="button" className="ml-4 text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 font-bold rounded-full text-sm p-2.5 text-center inline-flex items-center me-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800">
                                            +
                                        </button>
                                        <button onClick={() => editQuantity(cd.cartDetail,false)} type="button" className="text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-bold rounded-full text-sm p-2.5 text-center inline-flex items-center me-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800">
                                            -
                                        </button>
                                    </div>
                                </li>
                            )
                        })}
                        <li className="py-3 sm:py-4">
                            <div className="flex items-center">
                                <div className="flex-1 min-w-0 ms-4">
                                    <p className="text-sm font-medium text-gray-900 truncate dark:text-white">
                                        Total
                                    </p>
                                </div>
                                <div className="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                                    C$ {totalPriceProducts}
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default CartDetail;
