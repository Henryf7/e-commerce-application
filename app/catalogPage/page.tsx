'use client';
import React, { useEffect, useState } from 'react';
import { ICatalogPageProps, ICartDetail, ICartProducts } from '@/utils/interfaces/interfaces';
import Body from '@/components/Card/body';
import { postCartDetail } from '../services/products';
import { useRouter } from 'next/navigation';

const CatalogPage = ({ products, shoppingCartData, cartDetailsData } : ICatalogPageProps) => {
    const router = useRouter();
    const [searchTerm, setSearchTerm] = useState<string>('');
    const [cartItems, setCartItems] = useState<ICartProducts[]>([]);
    const [shoppingCart, setShoppingCart] = useState<ICartDetail[]>([]);
    const [cartId, setCartId] = useState<number>(0);

    useEffect(() => {
        if(shoppingCartData.length > 0)
            setCartId(shoppingCartData[0].id);
    },[])

    useEffect(() => {
        if (cartItems.length > 0) {
            const firstProduct = cartItems[0];
            postCartDetail(cartId, firstProduct.id, 1)
                .then((cartDetails) => {
                    setShoppingCart(cartDetails);
                })
                .catch((error) => {
                    console.error('Error al guardar los detalles del carrito:', error);
                });
        }
    }, [cartItems])

    const addToCart = (product: ICartProducts): void => {
        setCartItems(prevCart => [...prevCart, product]);
    };

    const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSearchTerm(e.target.value);
    };

    const productosFiltrados = products.filter((product) =>
        product.name.toLowerCase().includes(searchTerm.toLowerCase())
    );

    const onClick = () => {
        router.push(`/cartDetails/${shoppingCartData[0].id}`, { scroll: false });
    }

    return (
        <div className="p-4 md:p-8">
            <button onClick={onClick} type="button" className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800" >
                <svg className="w-6 h-6 text-gray-800 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" viewBox="0 0 24 24">
                    <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 4h1.5L9 16m0 0h8m-8 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4Zm8 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4Zm-8.5-3h9.25L19 7H7.312" />
                </svg>
            </button>
            <div className="mb-4">
                <input className="text-black" type="text" placeholder="Buscar por nombre de producto" onChange={handleSearch} />
            </div>
            <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-4">
                {productosFiltrados.map(product => {
                    return <Body
                        key={product.id}
                        available={product.available}
                        id={product.id}
                        name={product.name}
                        price={product.price}
                        img_url={product.img_url}
                        description={product.description}
                        addToCart={addToCart}
                    />
                })}
            </div>
        </div>
    );
};

export default CatalogPage;

