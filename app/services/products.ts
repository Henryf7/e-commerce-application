import { axiosClient } from "../../config/axios";
import { ICartDetail, IProduct, IShoppingCart } from "@/utils/interfaces/interfaces";

export const fetchAllProducts = async (): Promise<IProduct[]> => {
    const { data, status, request, error } = await axiosClient("GET", "/products");

    return (data ?? []) as IProduct[];
}

export const fetchShoppingCart = async (userId?: string): Promise<IShoppingCart[]> => {
    const { data, status, request, error } = await axiosClient("GET", "/shopping-carts", { userId });
    return (data ?? []) as IShoppingCart[];
}

export const postShoppingCart = async (userId?: string): Promise<IShoppingCart[]> => {
    const { data, status, request, error } = await axiosClient("POST", "/shopping-carts", { userId });

    return (data ?? []) as IShoppingCart[];
}

export const fetchCartDetail = async (cartId: number): Promise<ICartDetail[]> => {
    const { data, status, request, error } = await axiosClient("GET", `/cart-details/${cartId}`);

    return (data ?? []) as ICartDetail[];
}

export const postCartDetail = async (cartId: number, productId: number, quantity: number): Promise<ICartDetail[]> => {
    const { data, status, request, error } = await axiosClient("POST", "/cart-details", { cartId, productId, quantity });

    return (data ?? []) as ICartDetail[];
}

export const updateCartDetail = async (cartId: number, productId: number, quantity: number): Promise<ICartDetail[]> => {
    const { data, status, request, error } = await axiosClient("PATCH", "/cart-details", { cartId, productId, quantity });

    return (data ?? []) as ICartDetail[];
}