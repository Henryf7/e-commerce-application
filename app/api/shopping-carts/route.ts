import { createClient } from '@/utils/supabase/server';
import { NextRequest, NextResponse } from 'next/server';

export async function GET(request: NextRequest, context: any) {
    const supabase = createClient();

    // const {
    //     data: { user }, error: errorUser
    // } = await supabase.auth.getUser();

    // if (errorUser) {
    //     console.log(errorUser)
    //     return NextResponse.json({ error: 'Error: El usuario no existe', status: 500 });
    // }


    let { data: shopping_carts, error } = await supabase
        .from('shopping_carts')
        .select()
        .eq('user_id', '9e2d792b-b94a-41d7-97d1-6d4cb056e496'); // Cambiado de 'id' a 'user_id'

    if (error) {
        return NextResponse.json({ error: 'Error al obtener shopping_carts', status: 500 });
    }

    // Verificar si shopping_carts está vacío o nulo
    if (!shopping_carts || shopping_carts.length === 0) {
        return NextResponse.json({ data: [], status: 200 });
    }

    return NextResponse.json({ data: shopping_carts, status: 200 });
}

export async function POST(request: NextRequest, Context: any) {
    const supabase = createClient();

    const { userId } = await request.json()

    const { data: shopping_cart, error } = await supabase
        .from('shopping_carts')
        .insert([
            { user_id: userId },
        ])
        .select()

    if (error) {
        console.error('ERROR======>', error);
        return NextResponse.json({ error: 'Error al insertar shoppingCart', status: 500 });
    }

    return NextResponse.json({ data: shopping_cart, status: 200 });
}