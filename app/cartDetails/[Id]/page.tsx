import { ICartDetail, IProduct } from '@/utils/interfaces/interfaces';
import { fetchAllProducts, fetchCartDetail } from '../../services/products';
import CartDetail from '../details';

const CartDetailPage = async ({ params }: { params: { id: number } }) => {
    const { id } = params;

    const cartDetails: ICartDetail[] = await fetchCartDetail(id);
    const products: IProduct[] = await fetchAllProducts();

    const filteredProducts = products.filter(product => cartDetails.some(detail => detail.product_id === product.id));

    return (
        <div className="p-4 md:p-8">
            <CartDetail
                cartDetailData={cartDetails}
                productsData={filteredProducts}
            />
        </div>
    );
};

export default CartDetailPage;