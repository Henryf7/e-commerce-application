import { NextRequest, NextResponse } from "next/server";
import { createClient } from '@/utils/supabase/server';


export async function POST(request: NextRequest, Context: any) {
    const supabase = createClient();
    const formaData = await request.formData();

    const file = formaData.get('file');

    if (!file) {
        return NextResponse.json({ error: 'Error File not valid', status: 500 });
    }


    // Upload an image to the "avatars" bucket
    const { data, error } = await supabase
        .storage
        .from('images')
        .upload(`${Date.now()}-product.jpg`, file);

    return NextResponse.json({ data, status: 200 });
}