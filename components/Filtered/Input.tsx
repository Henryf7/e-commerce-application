'use client';
import { IProduct } from '@/utils/interfaces/interfaces';
import { useEffect, useState } from 'react';

const Input = async (props : IProduct[]) => {
    const [products, setProducts] = useState<IProduct[]>([]);
    const [searchQuery, setSearchQuery] = useState<string>(''); // Estado para almacenar la consulta de búsqueda

    useEffect(() => {
        setProducts(props);
    },[])

    // Filtrar productos según la consulta de búsqueda
    const filteredProducts = products.filter(product =>
        product.name.toLowerCase().includes(searchQuery.toLowerCase())
    );

    return (
        <div className="p-4 md:p-8">
            <input
                type="text"
                placeholder="Buscar productos..."
                value={searchQuery}
                onChange={e => setSearchQuery(e.target.value)}
            />
        </div>
    );
};

export default Input;