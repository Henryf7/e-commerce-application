import { createClient } from "@/utils/supabase/client";
import axios from 'axios';

export const axiosClient = async (method: string, path: string, data: any | null = null) => {
  const supabase = createClient();

  try {
    const response = await axios({
      method,
      url: `${process.env.NEXT_PUBLIC_API_URL}${path}`,
      headers: {
        Authorization: "Bearer eyJhbGciOiJIUzI1NiIsImtpZCI6IkRRc2xOUzB6V3hXalpibFMiLCJ0eXAiOiJKV1QifQ.eyJhdWQiOiJhdXRoZW50aWNhdGVkIiwiZXhwIjoxNzEzOTMxMTk1LCJpYXQiOjE3MTM5Mjc1OTUsImlzcyI6Imh0dHBzOi8vaW5semJ3bnZ6dXlidWhtd2J3emIuc3VwYWJhc2UuY28vYXV0aC92MSIsInN1YiI6IjllMmQ3OTJiLWI5NGEtNDFkNy05N2QxLTZkNGNiMDU2ZTQ5NiIsImVtYWlsIjoibGFnb3NqcDA3QGdtYWlsLmNvbSIsInBob25lIjoiIiwiYXBwX21ldGFkYXRhIjp7InByb3ZpZGVyIjoiZW1haWwiLCJwcm92aWRlcnMiOlsiZW1haWwiXX0sInVzZXJfbWV0YWRhdGEiOnsiZW1haWwiOiJsYWdvc2pwMDdAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwaG9uZV92ZXJpZmllZCI6ZmFsc2UsInN1YiI6IjllMmQ3OTJiLWI5NGEtNDFkNy05N2QxLTZkNGNiMDU2ZTQ5NiJ9LCJyb2xlIjoiYXV0aGVudGljYXRlZCIsImFhbCI6ImFhbDEiLCJhbXIiOlt7Im1ldGhvZCI6InBhc3N3b3JkIiwidGltZXN0YW1wIjoxNzEzOTI3NTk1fV0sInNlc3Npb25faWQiOiIyNGVjMjc4YS0wZTYxLTRjZWQtYTBjZS1iM2VhYTMwYWMyZjkiLCJpc19hbm9ueW1vdXMiOmZhbHNlfQ.ZkhiKh1K2Y-oDGBmClDfLYp3TqT-br8DZai8FsntByk",
      },
      data,
    });
    return response.data;
  } catch (error) {
    console.error('Error en la solicitud', error);
    throw error;
  }
} 
