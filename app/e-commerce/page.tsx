import CatalogPage from '../catalogPage/page';
import { ICartDetail, IProduct, IShoppingCart } from '@/utils/interfaces/interfaces';
import { fetchAllProducts, fetchCartDetail, fetchShoppingCart, postShoppingCart } from '../services/products';
import { redirect } from 'next/navigation';
import { createClient } from '@/utils/supabase/server';

const ECommerce = async () => {
    const supabase = createClient();
    const products: IProduct[] = await fetchAllProducts();
    const {
        data: { user },
    } = await supabase.auth.getUser();
    const userId = user?.id;

    if (!user) {
        return redirect("/login");
    }

    const shoppingCart: IShoppingCart[] = await fetchShoppingCart("9e2d792b-b94a-41d7-97d1-6d4cb056e496");
    
    if (shoppingCart.length == 0) {
        await postShoppingCart(userId);
    }

    const cartDetails: ICartDetail[] = await fetchCartDetail(shoppingCart[0].id);

    return (
        <div className="p-4 md:p-8">
            <CatalogPage 
                products={products}
                cartDetailsData={cartDetails}
                shoppingCartData={shoppingCart}
            />
        </div>
    );
};

export default ECommerce;

