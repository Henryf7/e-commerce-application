import { createClient } from '@/utils/supabase/server';
import { NextRequest, NextResponse } from 'next/server';

export async function POST(request: Request, Context: any) {
    const supabase = createClient();
    // const {
    //     data: { user }, error: errorUser
    // } = await supabase.auth.getUser();

    // if (errorUser) {
    //     return NextResponse.json({ error: 'Error: El usuario no existe', status: 500 });
    // }

    const { cartId, productId, quantity } = await request.json();

    const { data: cart_details, error } = await supabase
        .from('cart_details')
        .insert([
            { cart_id: cartId, product_id:productId, quantity },
        ])
        .select()

    if (error) {
        console.error('ERROR======>', error);
        return NextResponse.json({ error: 'Error al actualizar datos las notas', status: 500 });
    }

    return NextResponse.json({ data: cart_details, status: 200 });
}

export async function PATCH(request: Request, Context: any) {
    const supabase = createClient();

    const { cartId, productId, quantity } = await request.json();

    console.log("entro",{cartId, productId, quantity})

    if (quantity !== undefined) {
        console.log("entro sumar")
        const { data: updatedCartDetails, error } = await supabase
            .from('cart_details')
            .update({ quantity })
            .eq('cart_id', cartId)
            .eq('product_id', productId);

        if (error) {
            console.error('ERROR======>', error);
            return NextResponse.json({ error: 'Error al actualizar la cantidad', status: 500 });
        }

        return NextResponse.json({ data: updatedCartDetails, status: 200 });
    } else {
        return NextResponse.json({ message: 'La cantidad no se proporcionó, no se realizaron cambios', status: 400 });
    }
}