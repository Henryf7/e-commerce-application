import { createClient } from '@/utils/supabase/server';
import { NextRequest, NextResponse } from 'next/server';

type Context = {
    params: {
        id: number;
    };
};

export async function GET(request: NextRequest, context: Context) {
    const supabase = createClient();

    const cartId = context.params.id;

    const { data: cart_details, error } = await supabase
        .from('cart_details')
        .select()
        .eq('cart_id', cartId);

    if (error) {
        return NextResponse.json({ error: error, status: 500 });
    }

    // Verificar si shopping_carts está vacío o nulo
    if (!cart_details || cart_details.length === 0) {
        return NextResponse.json({ data: [], status: 200 });
    }

    return NextResponse.json({ data: cart_details, status: 200 });
}

export async function POST(request: Request, Context: any) {
    const supabase = createClient();
    // const {
    //     data: { user }, error: errorUser
    // } = await supabase.auth.getUser();

    // if (errorUser) {
    //     return NextResponse.json({ error: 'Error: El usuario no existe', status: 500 });
    // }

    const { cartId, productId, quantity } = await request.json();

    const { data: cart_details, error } = await supabase
        .from('cart_details')
        .insert([
            { cart_id: cartId, product_id:productId, quantity },
        ])
        .select()

    if (error) {
        console.error('ERROR======>', error);
        return NextResponse.json({ error: 'Error al actualizar datos las notas', status: 500 });
    }

    return NextResponse.json({ data: cart_details, status: 200 });
}