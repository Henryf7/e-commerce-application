// Interfaz para la tabla "users"
export interface IUser {
    id: number;
    name: string;
    address: string;
    email: string;
    password: string;
}

// Interfaz para la tabla "products"
export interface IProduct {
    id: number;
    name: string;
    description?: string;
    price: number;
    available: boolean;
    img_url: string;
}

// Interfaz para la tabla "shopping_carts"
export interface IShoppingCart {
    id: number;
    user_id: string;
    bought: boolean;
    creation_date: Date;
}

// Interfaz para la tabla "cart_details"
export interface ICartDetail {
    id: number;
    cart_id: number;
    product_id: number;
    quantity: number;
}

// Interfaz para la tabla "orders"
export interface IOrder {
    id: number;
    user_id: number;
    cart_id: number;
    total: number;
    shipping_address: string;
    payment_method: string;
    order_status: 'pending' | 'processed' | 'shipped' | 'delivered';
    order_date: Date;
}

// Interfaz para la tabla "purchase_history"
export interface IPurchaseHistory {
    id: number;
    user_id: number;
    product_id: number;
    purchase_date: Date;
}

// Interfaz para la tabla "product_recommendations"
export interface IProductRecommendation {
    id: number;
    user_id: number;
    product_id: number;
    recommendation_date: Date;
}

export interface IBodyProps extends IProduct {
    addToCart: (product: ICartProducts) => void;
}

export interface ICartProducts {
    id: number,
    name: string,
}

export interface ICartDetailProduct {
    cartDetail: ICartDetail;
    product: IProduct;
}

export interface ICatalogPageProps {
    products: IProduct[];
    shoppingCartData: IShoppingCart[];
    cartDetailsData: ICartDetail[];
}

export interface CartDetailProps {
    cartDetailData: ICartDetail[];
    productsData: IProduct[];
}
