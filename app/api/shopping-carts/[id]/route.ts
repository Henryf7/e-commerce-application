import { createClient } from '@/utils/supabase/server';
import { NextRequest, NextResponse } from 'next/server';

type Context = {
    params: {
        id: number;
    };
};

export async function PATCH(request: NextRequest, context: Context) {
    const supabase = createClient();

    const { bought } = await request.json();

    const { data, error } = await supabase
        .from('shopping_carts')
        .update({ bought: bought })
        .eq('id', context.params.id)
        .select()

    if (error) {
        console.error('ERROR======>', error);
        return NextResponse.json({ error: 'Error al actualizar shopping_cart', status: 500 });
    }

    return NextResponse.json({ data, status: 200 });
}